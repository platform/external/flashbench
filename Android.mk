LOCAL_PATH:= $(call my-dir)

flashbench_cflags := \
  -O2 \
  -Wall \
  -Wextra \
  -Wno-missing-field-initializers \
  -Wno-unused-parameter \
  -g2

include $(CLEAR_VARS)
LOCAL_CFLAGS := $(flashbench_cflags)
LOCAL_MODULE := flashbench
LOCAL_SRC_FILES := flashbench.c vm.c dev.c
LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_CFLAGS := $(flashbench_cflags)
LOCAL_MODULE := flashbench_erase
LOCAL_SRC_FILES := erase.c
LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)
